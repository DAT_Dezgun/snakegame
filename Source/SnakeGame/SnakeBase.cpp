// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Food.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(3);
	AddNewFood();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		if (SnakeElenemts.Num() == 0)
		{
			FVector NewLocation(SnakeElenemts.Num() * ElementSize, 0, 0);
			FTransform NewTransform(NewLocation);
			ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
			NewSnakeElem->SnakeOwner = this;
			int32 ElemIndex = SnakeElenemts.Add(NewSnakeElem);
			if (ElemIndex == 0)
			{
				NewSnakeElem->SetFirstElementType();
			}
		}
		else
		{
			FVector NewLocation = SnakeElenemts.Top()->GetActorLocation();
			FTransform NewTransform(NewLocation);
			ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
			NewSnakeElem->SnakeOwner = this;
			int32 ElemIndex = SnakeElenemts.Add(NewSnakeElem);
			if (ElemIndex == 0)
			{
				NewSnakeElem->SetFirstElementType();
			}
		}
		
	}
	
}

void ASnakeBase::AddNewFood()
{
	float MinY = -950.f;	float MaxY = 950.f;
	float MinX = -500.f;	float MaxX = 500.f;
	float y = FMath::FRandRange(MinY, MaxY);
	float x = FMath::FRandRange(MinX, MaxX);
	FVector SpawnLocation(x, y, 0);
	FTransform NewTransform(SpawnLocation);
	AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
	NewFood->SetFoodType();
}

void ASnakeBase::MoveSpeedUp()
{
	MovementSpeed = MovementSpeed * 0.8;
	SetActorTickInterval(MovementSpeed);
}

void ASnakeBase::MoveSpeedDown()
{
	MovementSpeed = MovementSpeed * 1.2;
	SetActorTickInterval(MovementSpeed);
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElenemts[0]->ToggleCollision();

	for (int i = SnakeElenemts.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElenemts[i];
		auto PrevElement = SnakeElenemts[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElenemts[0]->AddActorWorldOffset(MovementVector);
	SnakeElenemts[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElenemts.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

