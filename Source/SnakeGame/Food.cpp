// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"


// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateAbstractDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

	FastMaterial = CreateAbstractDefaultSubobject<UMaterial>(TEXT("FastFoodMaterial"));
	SlowMaterial = CreateAbstractDefaultSubobject<UMaterial>(TEXT("SlowFoodMaterial"));
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			if ((FastFood == false) && (Snake->MovementSpeed <= 1.f))
			{
				Snake->MoveSpeedDown();
			}
			if ((FastFood == true) && (Snake->MovementSpeed >= 0.15))
			{
				Snake->MoveSpeedUp();
			}
			Snake->AddNewFood();
			Destroy();
		}
	}
}



void AFood::SetFoodType()
{
	
	int a = rand() % (10 + 10 + 1) - 10;
	if (a >= 0)
	{
		FastFood = true;
		MeshComponent->SetMaterial(0, FastMaterial);
	}
	else
	{
		FastFood = false;
		MeshComponent->SetMaterial(0, SlowMaterial);
	}
}

